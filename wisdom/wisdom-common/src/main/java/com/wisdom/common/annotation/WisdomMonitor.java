package com.wisdom.common.annotation;


import java.lang.annotation.*;

/**
 * Created by yang_hx on 2017/9/26.
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface WisdomMonitor {
    String[] value() default {};
}
